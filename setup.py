from setuptools import setup, find_packages

setup(name="FormatToDictionary",
      version="0.12",
      packages=find_packages(exclude=['*tests']),
      include_package_data=True,
      package_data={
          'sample': ['package_data.py'],
      },
      description="Package to convert yaml, .cfg, .config file contain to dictionary",
      author="Anuj Patil",
      zip_safe=False,
      install_requires=['attrs>=21.2.0'],
      classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
      ],
      )
