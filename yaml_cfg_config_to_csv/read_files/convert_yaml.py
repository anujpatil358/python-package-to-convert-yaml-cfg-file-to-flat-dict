import yaml
import os
import json
import pandas as pd
from pprint import pprint

data = {}
conf = []


def flatten_dict(yaml_data, sep: str = '_'):
    flat_dict = pd.json_normalize(yaml_data, sep=sep).to_dict(orient='records')[0]
    keys = None
    dd = {}
    pprint(flat_dict)
    for fd in flat_dict:
        keys = fd

        if type(flat_dict[fd]) == str or type(flat_dict[fd]) == int:
            dd.update({keys: flat_dict[fd]})

        if type(flat_dict[fd]) == list:
            for fd2 in flat_dict[fd]:
                if type(fd2) == str or type(fd2) == int:
                    dd.update({keys: flat_dict[fd]})

                if type(fd2) == dict:
                    for k, v in fd2.items():
                        keys = keys + '_' + k
                        dd.update({keys + '=': v})
    return dd


def convert_yaml_to_dict(path):
    if os.path.exists(path):
        try:
            stream = open(path, 'r')
            dictionary = yaml.safe_load(stream)
            flatten_data = flatten_dict(dictionary)
            for k, v in flatten_data.items():
                new_data = k + '=' + str(v) + '\n'
                set_os_env(key=k, value=str(v))
                conf.append(new_data)
            if 'convertor' in flatten_data:
                file_to_convert = flatten_data['convertor']
                config_file(file_type=file_to_convert, dict_data=dictionary, conf_data=conf)
            else:
                return "Please provide convertor(env, json, os)"
            return flatten_data
        except Exception as e:
            return e

    elif os.access(os.path.dirname(path), os.W_OK):
        return "The file does not exists in that directory."
    else:
        return "The file path does not exists."


def config_file(dict_data, conf_data, file_type='env'):
    types = ['env', 'json', 'os']
    if file_type in types:
        if file_type == 'env':
            file_name = '.env'
            convert_to_env_file(file_name, conf_data)

        if file_type == 'json':
            file_name = 'config.json'
            convert_to_json_file(file_name, dict_data)
    else:
        return "Please provide proper convertor"


def convert_to_json_file(file_name, json_data):
    new_json_data = json.dumps(json_data, indent=4)
    with open(file_name, 'w') as file:
        file.write(new_json_data)
    file.close()


def convert_to_env_file(file_name, data):
    with open(file_name, 'w') as file:
        file.write("".join(data))
    file.close()


def set_os_env(key, value):
    os.environ[key] = value
    print(key, os.getenv(key))
