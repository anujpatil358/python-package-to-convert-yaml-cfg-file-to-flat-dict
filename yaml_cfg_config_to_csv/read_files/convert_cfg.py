import configparser
import os
import json
from string import Template

dictionary = {}
conf = []


def convert_cfg_to_dict(path):
    file_to_convert = None
    if os.path.exists(path):
        config = configparser.ConfigParser()
        config.read(path)
        config.sections()
        for section in config.sections():
            # dictionary[section] = {}
            for option in config.options(section):
                if section == 'convertor':
                    file_to_convert = config.get(section, option)
                dictionary[section + '_' + option] = config.get(section, option)
                set_os_env(key=section + '_' + option, value=config.get(section, option))
                data = section + '_' + option + '=' + config.get(section, option) + '\n'
                conf.append(data)

        config_file(file_type=file_to_convert, dict_data=dictionary, conf_data=conf)

        response = Template("File successfully converted into $name")
        response = response.substitute(name=file_to_convert)
        return response

    elif os.access(os.path.dirname(path), os.W_OK):
        return "The file does not exists in that directory."
    else:
        return "The file path does not exists."


def config_file(dict_data, conf_data, file_type='env'):
    types = ['env', 'json', 'os']
    file_name = None
    if file_type in types:
        if file_type == 'env':
            file_name = '.env'
            convert_to_env_file(file_name, conf_data)

        if file_type == 'json':
            file_name = 'config.json'
            convert_to_json_file(file_name, dict_data)
    else:
        return "Please provide proper convertor"


def convert_to_json_file(file_name, data):
    json_data = json.dumps(data, indent = 4)
    with open(file_name, 'w') as file:
        file.write(json_data)
    file.close()


def convert_to_env_file(file_name, data):
    with open(file_name, 'w') as file:
        file.write("".join(data))
    file.close()


def set_os_env(key, value):
    os.environ[key] = value
    print(key, os.getenv(key))


