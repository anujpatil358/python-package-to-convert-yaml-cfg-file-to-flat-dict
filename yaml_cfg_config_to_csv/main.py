try:

    import os
    from pprint import pprint
    from read_files.convert_yaml import convert_yaml_to_dict
    from read_files.convert_cfg import convert_cfg_to_dict

except Exception as e:

    from string import Template

    temp = Template("Some packages are missing $name")
    msg = temp.substitute(name=e)
    print(msg)

# file_path = "./data/read.yaml"
file_path = "./data/new.cfg"


def convert_to_dict():
    # Check path of file is proper or not
    if os.path.exists(file_path):
        # Getting a file name and extension
        name, extension = os.path.splitext(file_path)

        # For yaml file
        if extension == ".yaml":
            try:
                data = convert_yaml_to_dict(file_path)
                return data
            except Exception as e:
                return e

        # For config(.cfg) file
        if extension == ".cfg":
            try:
                data = convert_cfg_to_dict(file_path)
                return data
            except Exception as e:
                return e

        return "File format should be .yaml, .cfg, .conf"
    elif os.access(os.path.dirname(file_path), os.W_OK):
        return "The file does not exists in that directory."
    else:
        return "The file path does not exists."


if __name__ == "__main__":
    pprint(convert_to_dict())
