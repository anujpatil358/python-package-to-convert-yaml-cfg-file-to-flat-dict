from yaml_cfg_config_to_csv.read_files import convert_yaml_to_dict


def test_with_correct_path():
    file_path = "../yaml_cfg_config_to_csv/data/test_data/read.yaml"
    output = convert_yaml_to_dict(file_path)
    assert output == {'deliverables': [{'name': 'HTML5'}], 'includes': ['common.yaml']}


def test_with_incorrect_directory():
    file_path = "../yaml_cfg_config_to_csv/data/test_dat/read.yaml"
    output = convert_yaml_to_dict(file_path)
    assert output == "The file path does not exists."


def test_with_incorrect_path():
    file_path = "../yaml_cfg_config_to_csv/data/test_data/reada.yaml"
    output = convert_yaml_to_dict(file_path)
    assert output == "The file does not exists in that directory."
