from yaml_cfg_config_to_csv.read_files import convert_cfg_to_dict


def test_with_correct_path():
    file_path = "../yaml_cfg_config_to_csv/data/test_data/new.cfg"
    output = convert_cfg_to_dict(file_path)
    print('anuj', output)
    assert output == 'File successfully converted into env'


def test_with_incorrect_directory():
    file_path = "../yaml_cfg_config_to_csv/data/test_dat/new.cfg"
    output = convert_cfg_to_dict(file_path)
    assert output == "The file path does not exists."


def test_with_incorrect_path():
    file_path = "../yaml_cfg_config_to_csv/data/test_data/newa.cfg"
    output = convert_cfg_to_dict(file_path)
    assert output == "The file does not exists in that directory."
